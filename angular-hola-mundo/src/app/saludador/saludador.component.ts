import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { NEVER } from 'rxjs';
/*
import { destinoViaje } from '../models/destino-viaje.model';
*/
import { destinoViaje } from '../../app/models/destino-viaje.model';


@Component({
  selector: 'app-saludador',
  templateUrl: './saludador.component.html',
  styleUrls: ['./saludador.component.css']
})
export class SaludadorComponent implements OnInit {
  @Input() destino?: destinoViaje;
  @HostBinding(`attr.class`) cssClass = `col-md-4`;
  constructor() {
    
  }

  ngOnInit(): void {
  }

}
